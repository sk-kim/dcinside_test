//
//  APIManager.swift
//  dcinside_test
//
//  Created by USER on 2017. 4. 4..
//  Copyright © 2017년 USER. All rights reserved.
//

import UIKit
import Foundation
import Alamofire


@objc
protocol APIManagerDelegate_old {
   @objc optional func receive_json(json:Any)
}

class APIManager_old {
    
    typealias APICompletion = ((Data?, URLResponse?, Error?) -> Void)
    var delegate: APIManagerDelegate?
    
    
    /**
     * APP 이용 허가 정보
     * [
     *  {
     *     "result": true,
     *     "ver": "1.2.3",
     *     “notice”: true,
     *     “notice_update”:false
     *  }
     * ]
     *
     */
    func app_check() {
        
        let targetURL = "https://json.dcinside.com/App/app_check_I_rina.php"
        
        let callback = { (data:Data?, respones:URLResponse?, error:Error?) in
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: []) as! [[String:Any]]
                
                self.delegate?.receive_json!(json: json, type: "app_check")
                //print(json)
                
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        
        call_api(url: targetURL, httpMethod: "GET", completion: callback)
    }
    
    
    func call_api(url:String, httpMethod:String, completion:@escaping APICompletion){
        let headers:HTTPHeaders = ["User-Agent": "dcinside.app"]
        
        Alamofire.request(url, headers:headers).responseJSON { response in
            print(response.request!)  // original URL request
            print(response.response!) // HTTP URL response
            print(response.data!)     // server data
            print(response.result)   // result of response serialization
            
            if let JSON = response.result.value {
                //print("JSON: \(JSON)")
                //self.delegate?.receive_json!(json: JSON)
            }
        }
    }
    
    
    
    func call_api2(url:String, httpMethod:String, completion:@escaping APICompletion){
        let baseURL = URL(string: url)!
        var request = URLRequest(url:baseURL)
        let session = URLSession.shared
        
        request.httpMethod = httpMethod
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        request.setValue("dcinside.app", forHTTPHeaderField: "User-Agent")
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            guard error == nil else {
                print(error.debugDescription)
                return
            }
            
            guard let data = data else {
                print("Data is empty")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
                return
            }
            
            return completion(data, response, error)
        })
        
        task.resume()
    }
}
