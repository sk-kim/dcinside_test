//
//  APIManager.swift
//  dcinside_test
//
//  Created by USER on 2017. 4. 4..
//  Copyright © 2017년 USER. All rights reserved.
//


import Foundation
import Alamofire


@objc
protocol APIManagerDelegate {
    @objc optional func receive_json(json:Any?, type:String)
}

class APIManager {
    
    
    var delegate: APIManagerDelegate?
    var app_id:String? = ""
    var enc_usser_id:String? = ""
    
    
    init() {
        if(self.app_id == "" || self.app_id == nil) {
            self.app_key_verification()
        }
    }
    
    /**
     * APP 이용 허가 정보 API Doc (1)
     *
     */
    func app_check() {
        let url = "http://json.dcinside.com/App/app_check_I_rina.php"
        
        get_simply_json(targetURL:url, httpMethod:HTTPMethod.get, apiType:"app_check")
    }
    
    
    /**
     * 전체 갤러리 정보   API Doc (2)
     *
     *
     */
    func get_gallery() {
        let url = "http://json.dcinside.com/App/gall_name.php"
        
        get_simply_json(targetURL:url, httpMethod:HTTPMethod.get, apiType: "get_gallery")
    }

    
    /**
     * 로그인 처리 API Doc (3)
     *  - 로그인이 성공하면 인코딩된 user_id를 저장한다.
     *
     */
    func app_login(user_id:String, user_pw:String) {
        let url = "http://dcid.dcinside.com/join/mobile_app_login.php"
        var param:Parameters = [String:String]()
        
        param["user_id"] = user_id
        param["user_pw"] = user_pw
        
        get_simply_json(targetURL:url, httpMethod:HTTPMethod.post, postParam:param, apiType: "app_login")
    }
    
    // 로그인 후 암호화된 아이디 저장
    func setUserId(data:Any) {
        let json = data as! [[String:Any]]
        let json_item = json[0]

        let result = json_item["result"] as! Bool
        
        if(result) {
            self.enc_usser_id = json_item["user_id"] as? String
        }
    }

 
    /**
     * 갤러리의 게시물 리스트.  API Doc (4)
     *
     *
     */
    func get_gallery_list(id:String, page:Int, serVal:String="", s_type:String="", notice:Int=0, recommend:Int=0) {
        
        let pre_url = "http://m.dcinside.com/api/gall_list.php?id=\(id)&page=\(page)&serVal=\(serVal)&s_type=\(s_type)&notice=\(notice)&best=\(recommend)&app_id=\(self.app_id!)"
        
        let base64_param = self.base64(input: pre_url)
        let url = "http://m.dcinside.com/api/redirect.php?hash=\(base64_param)"
        
        get_simply_json(targetURL:url, httpMethod:HTTPMethod.get, apiType: "get_gallery_list")
    }

    
    /**
     * 갤러리의 내용 보기 상단.  API Doc (5)
     *
     *
     */
    func view_gallaery_top_menu(id:String, page:Int=0, no:Int) {
        let pre_url = "http://m.dcinside.com/api/gall_view.php?id=\(id)&no=\(no)&page=\(page)&app_id=\(self.app_id!)"
        
        let base64_param = self.base64(input: pre_url)
        let url = "http://m.dcinside.com/api/redirect.php?hash=\(base64_param)"
        
        get_simply_json(targetURL:url, httpMethod:HTTPMethod.get, apiType: "view_gallaery_top_menu")
        
    }

    
    /**
     * 갤러리 게시물 작성 및 수정.  API Doc (6)
     *
     *
     */
    func set_gall_write () {
        
    }
    
    
    /**
     * 갤러리 게시물 내용 수정.  API Doc (7)
     *
     *
     */
    func set_gall_modify () {
        
    }
    
    
    /**
     * 갤러리 댓글 리스트.  API Doc (8)
     *
     * !TODO
     */
    func get_gall_comment_list (id:String, no:Int, re_page:Int) {
        let pre_url = "http://m.dcinside.com/api/comment.php?id=\(id)&no=\(no)&re_page=\(re_page)&app_id=\(self.app_id!)"
        
        let base64_param = self.base64(input: pre_url)
        let url = "http://m.dcinside.com/api/redirect.php?hash=\(base64_param)"
        
        get_simply_json(targetURL:url, httpMethod:HTTPMethod.get, apiType: "get_gall_comment_list")
    }
    
    
    /**
     * 갤러리 댓글 입력.  API Doc (9)
     *
     *
     */
    func set_gall_comment_write () {
        
    }
    
    
    /**
     * 갤러리 댓글 삭제.  API Doc (10)
     *
     *
     */
    func do_gall_comment_delete () {
        
    }
    
    
    /**
     * 갤러리 게시글 삭제.  API Doc (11)
     *
     *
     */
    func do_gall_delete () {
        
    }
    
    
    /**
     * 마이 갤러리 및 즐겨찾기 리스트.  API Doc (12)
     *
     *  TODO
     */
    func get_mygall_and_favorite () {
        let url = "http://m.dcinside.com/api/mygall.php"
        var param:Parameters = [String:String]()
        
        if(self.enc_usser_id == "" || self.enc_usser_id == nil) {
            print("로그인 정보가 없음!!")
            return
        }
        
        param["user_id"] = self.enc_usser_id
        param["app_id"] = self.app_id
        
        get_simply_json(targetURL:url, httpMethod:HTTPMethod.post, postParam:param, apiType: "get_mygall_and_favorite")

    }
    
    
    /**
     * 마이 갤러리 및 즐겨찾기 설정.  API Doc (13)
     *
     *  TODO
     */
    func set_mygall_modify () {
        
    }
    
    
    /**
     * 갤러리 본문.  API Doc (14)
     *
     *  TODO
     */
    func get_gallery_view () {
        
    }
    
    
    /**
     * 메인 에서 사용 되는 HIT, 신설 갤, 초개념 컨텐츠.  API Doc (15)
     *
     *  
     */
    func get_main_contents () {
        let url = "http://json.dcinside.com/App/main_content.php"
        
        get_simply_json(targetURL:url, httpMethod:HTTPMethod.get, apiType: "get_main_contents")
    }
    
   
    /**
     * 갤러리 본문 이미지 보기.  API Doc (16)
     *   - 갤러리 본문 첨부 이미지 클릭 보기
     *
     */
    func get_view_img () {
        
    }
    
    
    /**
     * 갤러리 신고.  API Doc (17)
     *
     *
     */
    func do_gallery_report () {
        
    }
    
    
    /**
     * 전체 카테고리 이름  API Doc (19)
     *
     *
     */
    func get_category() {
        let url = "http://json.dcinside.com/App/category_name.php"
        
        get_simply_json(targetURL:url, httpMethod:HTTPMethod.get, apiType: "get_category")
    }
    
    
    /**
     * 보이스 리플 등록.  API Doc (20)
     *
     *
     */
    func set_voice_reply_upload () {
        
    }
    
    
    /**
     * 디시콘 노출.  API Doc (21)
     *   - 내용 확인해야 함
     *
     */
    
    
    
    /**
     * 디시콘 리스트.  API Doc (22)
     *  - 사용 가능한 디시콘 정보 및 리스트 (댓글 입력 , 글쓰기 입력 가능)
     *
     */
    func get_dcon_list () {
        
    }
    
    
    /**
     * 최근 사용 디시콘 리스트.  API Doc (23)
     *  - 사용 가능한 디시콘 정보 및 리스트 (댓글 입력 , 글쓰기 입력 가능)
     *
     */
    func get_recent_dcon_list () {
        
    }
    
    
    /**
     * 디시콘 사용/입력.  API Doc (24)
     *  - 디시콘 이미지 클릭 시 실행 (댓글 입력 , 글쓰기 입력 가능)
     *
     */
    func do_insert_dcon () {
        
    }
    
    
    /**
     *  실시간 북적 갤러리.  API Doc (25)
     *  - 메인 실시간 북적 갤러리 리스트 정보
     *
     */
    func get_ranking_gallery () {
        let url = "http://json.dcinside.com/App/ranking_gallery.php"
        
        get_simply_json(targetURL:url, httpMethod:HTTPMethod.get, apiType:"get_ranking_gallery")

    }
    
    
    /**
     *  게시글 추천.  API Doc (26)
     *  - 갤러리 게시글 추천
     *
     */
    func set_gallery_recommend_up () {
        
    }
    
    
    /**
     *  게시글 비추천.  API Doc (27)
     *  - 갤러리 게시글 비추천
     *
     */
    func set_gallery_recommend_down () {
        
    }

    
    /**
     *  마이너 갤러리.  API Doc (28)
     *  - 전체 카테고리 이름
     *
     */
    func get_minor_category_name () {
        
    }
    
    
    
    /**
     *  마이너 전체 갤러리.  API Doc (29)
     *  - 전체 마이너 갤러리의 ‘카테고리 고유번호’, ‘갤러리 아이디’, ‘갤러리명’, ‘갤러리 고유번호’, ’매니저,’부매니저’ 등 정보
     *  - 주기적으로 변동이 있는 만큼 최소 하루 한번 로컬 스토리지 업데이트 필요
     */
    func get_minor_gallery () {
        
    }
    
    
    /**
     *  APP 업데이트 안내 공지.  API Doc (30)
     *  - 업데이트 내용 안내
     *
     */
    func get_update_info () {
        
    }
    
    
    /**
     *  전체 안내 공지.  API Doc (31)
     *  - 전체 안내 공지 내용
     *
     */
    func get_notice () {
        
    }
    
    
    /**
     *  검색 광고 호출.  API Doc (32)
     *  - 갤러리 글 검색 광고 호출
     *
     */
    func get_ad_search_link () {
        
    }
    
    
    /**
     *  광고 노출 관리.  API Doc (33)
     *  - 종료광고, 스와이프 광고 등 on 설정값
     *
     */
    func get_ad_setting () {
        
    }
    
    
    /**
     *  디시콘 구입 레이어 정보 보기.  API Doc (34)
     *  - 댓글 의 디시콘 클릭시 구입 레이어 정보
     *
     */
    func get_dccon_layer_info () {
        
    }
    
    
    /**
     *  디시콘 구입.  API Doc (35)
     *  - 디시콘 댓글 클릭 정보 레이어 창 에서 구입
     *
     */
    func do_buy_dccon () {
        
    }
    
    
    /**
     * API 발급키 인증.  API Doc (36)
     *
     *  -
     */
    func app_key_verification() {
        let url = "https://dcid.dcinside.com/join/mobile_app_key_verification_3rd.php"
        var param:Parameters = [String:String]()
        
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMddHH"
        
        let chkString = "dcArdchk_" + dateFormatter.string(from: date)
        let shaString = sha256(inputStr: chkString)
        
        param["value_token"] = shaString
        param["signature"] = "Ah54t065GYTinrJ2kzeXKjNDhHvqamsbLNPrawKJ3ds="
        param["pkg"] = "com.dcinside.app"
        param["vCode"] = "10802"
        param["vName"] = "1.8.2"
        
        get_simply_json(targetURL:url, httpMethod:HTTPMethod.post, postParam:param, apiType: "app_key_verification")
    }
    
    // 인증 후 앱 키 저장
    func setAppId(data:Any) {
        let json = data as! [[String:Any]]
        
        for json_item in json {
            let result = json_item["result"] as! Bool
        
            if(result) {
                self.app_id = json_item["app_id"] as? String
            }
        }
        
    }
    
    
    /**
     *  개별앱 연관갤러리.  API Doc (37)
     *
     *
     */
    func get_relation_gallery_list () {
        
    }
    
    
    /**
     *  타 갤러리 개념 컨텐츠.  API Doc (38)
     *
     *
     */
    func get_notion_contents () {
        
    }
    
    
    
    
    /********************************************************************************************************
     * 서버 API 호출
     *
     *
     */
    func get_simply_json(targetURL:String, httpMethod:HTTPMethod, postParam:Parameters? = nil, apiType:String){
        var headers:HTTPHeaders = [String:String]()
        
        headers["User-Agent"] = "dcinside.app"
        
        Alamofire.request(targetURL, method:httpMethod, parameters:postParam, headers:headers).responseJSON { response in
            //print(response.debugDescription)  // original URL request
            //print(response.result)   // result of response serialization
            
            if let JSON = response.result.value {
                print("============ get_simply_json =================")
                print(JSON)
                
                if apiType == "app_login" {
                    self.setUserId(data: JSON)
                }
                else if apiType == "app_key_verification" {
                    self.setAppId(data: JSON)
                }
                
                self.delegate?.receive_json?(json: JSON, type: apiType)
            }
            else {
                self.delegate?.receive_json?(json: nil, type: apiType)
            }
        }
    }
    
    
    
    /**
     * SHA 256
     *
     *
     */
    func sha256(inputStr:String) -> String{
        if let stringData = inputStr.data(using: String.Encoding.utf8) {
            return hexStringFromData(input: digest(input: stringData as NSData))
        }
        return ""
    }
    
    private func digest(input : NSData) -> NSData {
        let digestLength = Int(CC_SHA256_DIGEST_LENGTH)
        var hash = [UInt8](repeating: 0, count: digestLength)
        CC_SHA256(input.bytes, UInt32(input.length), &hash)
        return NSData(bytes: hash, length: digestLength)
    }
    
    private  func hexStringFromData(input: NSData) -> String {
        var bytes = [UInt8](repeating: 0, count: input.length)
        input.getBytes(&bytes, length: input.length)
        
        var hexString = ""
        for byte in bytes {
            hexString += String(format:"%02x", UInt8(byte))
        }
        
        return hexString
    }

    
    /**
     *
     *
     */
    func base64(input:String) -> String {
        return Data(input.utf8).base64EncodedString()
    }
        
}
