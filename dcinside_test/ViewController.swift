//
//  ViewController.swift
//  dcinside_test
//
//  Created by USER on 2017. 4. 4..
//  Copyright © 2017년 USER. All rights reserved.
//

import UIKit

class ViewController: UIViewController, APIManagerDelegate {

    @IBOutlet weak var btnRun: UIButton!
    @IBOutlet weak var btnGallery: UIButton!
    @IBOutlet weak var btnCategory: UIButton!
    @IBOutlet weak var btnCategoryList: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnViewTopMenu: UIButton!
    @IBOutlet weak var btnAppkey: UIButton!
    
    
    let api = APIManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.api.delegate = self
    }

    
    @IBAction func clickRun(_ sender: Any) {
        self.api.app_check()
    }
    
    @IBAction func get_gallery(_ sender: Any) {
        self.api.get_gallery()
    }
    
    
    @IBAction func get_category(_ sender: Any) {
        self.api.get_category()
    }
    
    
    @IBAction func get_gallery_list(_ sender: Any) {
        
        self.api.get_gallery_list(id: "superidea", page: 1)
    }
    
    
    @IBAction func login(_ sender: Any) {
        self.api.app_login(user_id: "old2new", user_pw: "qazwsx123")
    }
    
    
    @IBAction func view_top_menu(_ sender: Any) {
        self.api.view_gallaery_top_menu(id: "superidea", no: 104562)
    }
    
    @IBAction func get_appkey(_ sender: Any) {
        self.api.get_gall_comment_list(id:"superidea", no: 104562, re_page: 2)
    }
    
    
    func receive_json(json: Any?, type:String) {
        print("Delegate........ \(type)")
        
        if type == "get_gallery_list" {
            let data = json as? [Any]
            
            let data2 = data?[0] as! [String:[[String:Any]]]
        
            let gall_info = data2["gall_info"]!
            let gall_list = data2["gall_list"]!
            
            let gall_info_item = gall_info[0]
            print(gall_info_item)
            
            for list in gall_list {
                print(list)
            }
        }
        else if type == "get_mygall_and_favorite"{
            let data = json as? [Any]
            
            let data2 = data?[0] as! [String:[[String:Any]]]
            let favori = data2["favori"]!
            let mygall = data2["mygall"]!
            
            let favori_item = favori[0]
            print(favori_item)
            
            for gall in mygall {
                print(gall)
            }
        }
        else if type == "get_main_contents" {
            let data = json as? [Any]
            
            let data2 = data?[0] as! [String:[[String:Any]]]
            
            guard let best = data2["best"] else {
                print("best data empty")
                return
            }
            guard let hit = data2["hit"] else {
                print("hit data empty")
                return
            }
            guard let new_gallery = data2["new_gallery"] else {
                print("new_gallery data empty")
                return
            }
            
            print("=========best===================")
            print(best)
            
            print("=========hit===================")
            print(hit)
            
            print("=========new gallery===================")
            print(new_gallery)
            
        }
        else if type == "get_gall_comment_list" {
            let data = json as? [Any]
            
            let data2 = data?[0] as! [String:Any]
            
            let re_page = data2["re_page"] as! String
            let total_comment = data2["total_comment"] as! String
            let total_page = data2["total_page"] as! String
            let comment_list = data2["comment_list"] as! [[String:Any]]
            
            print(re_page)
            print(total_comment)
            print(total_page)
            
            print(comment_list)
            
        }
        else {
            let data = json as? [[String:Any]]
            
            for d in data! {
                print(d)
            }
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

