//
//  DataModel.swift
//  dcinside_test
//
//  Created by USER on 2017. 4. 4..
//  Copyright © 2017년 USER. All rights reserved.
//

import Foundation
import RealmSwift

/**
 * 갤러리 정보
 *
 */
class Gallery: Object {
    /**
     *  갤리리 번호
     */
    dynamic var number = 0
    
    /**
     * 갤러리 카테고리 번호
     */
    dynamic var category = 0
    
    /**
     * 갤러리 아이디
     */
    dynamic var id = ""
    
    /**
     * 갤러리 이름
     */
    dynamic var name = ""
    
    /**
     * 게시물 등록 차단 여부
     */
    dynamic var isBlockingWrite = false
    
    /**
     * 성인 여부
     */
    dynamic var isAdult = false
    
    /**
     * 여성 구분 (1:여성, 2:성인여성)
     */
    dynamic var ladyTyep = 0
    
    /**
     * 마이너 갤러리 여부
     * 값이 없으면 마이너 갤러리, up이면 지상 갤러리, down이면 지하 갤러리
     */
    dynamic var mnGallery = ""
    
    /**
     * 마이너 갤러리 일 경우 갤러리 매니져 아이디
     */
    dynamic var mnGalManager = ""
    
    /**
     * 마이너 갤러리 일 경우 갤러리 부매니져 아이디
     */
    dynamic var mnGalSubManager = ""
}

/**
 * 카테고리 정보
 *
 */

class Category: Object {
    /**
     * 카테고리 번호
     */
    dynamic var number = 0
    
    /**
     * 카테고리 이름
     */
    dynamic var name = ""
}

/**
 * 마이너 카테고리 정보
 *
 */

class MinorCategory: Object {
    /**
     * 카테고리 번호
     */
    dynamic var number = 0
    
    /**
     * 카테고리 이름
     */
    dynamic var name = ""
}
